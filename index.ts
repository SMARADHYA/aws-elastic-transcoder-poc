import AWS from "aws-sdk";
import { config } from "dotenv";

config();

const getVideos = async()=>{
    console.log("hi")
    const s3 = new AWS.S3();

    const items = await s3.listObjectsV2({
        Bucket:"test-transcoder-poc",
    }).promise();

console.log(items)

    if(items.Contents){
        return items.Contents.map((content)=>content.Key) as string[];
    }

    throw new Error("No Videos")

};

const transcodeVideos = async (videoArr:string[]) => {
    const transcoder = new AWS.ElasticTranscoder();

    for(const video of videoArr){
        const job = await transcoder.createJob({
            PipelineId:"1661834683276-6zdgxd",
            Input:{
                Key : video,
                Container:'mp4'
            },
            Output:{
                Key:`output/${video}`,
                PresetId:"1351620000001-000001",
            }
        }).promise();
    }
}

const main = async () =>{
    const videos = await getVideos();
    transcodeVideos(videos)
}

main();
